var bodyParser = require('body-parser');
var md5 = require('md5')
const express = require('express');
const fileUpload = require('express-fileupload');
const _ = require('lodash');
const cors = require('cors');
const app = express();
app.use(cors());
var sqlite3 = require('sqlite3').verbose();
const Database = require('better-sqlite3');
const db = new Database('db.sqlite', { verbose: console.log });
//var db = require("./database.js");
var token = require("./token.js");
var picture = require("./picture.js");
var blockchain = require("../blockchain/lib/blockchain.js");
var interface = require("../blockchain/exemple/interface.js");

// Parses the body for POST, PUT, DELETE, etc.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload({ createParentPath: true }));
app.use(cors());
//app.use(express.static('public'));
app.use('/pictures', express.static('pictures'));

app.get('/list', function(res, res, next) {
  var stmt = db.prepare("SELECT email FROM user");
  var rows = stmt.all();
  console.log(rows);
  res.status(200).json({"message": "success"});
});

app.post('/login', function(req, res){
  console.log("name : " + req.body.email + " passwd : " + req.body.password);
  var stmt_select = db.prepare("SELECT id, name, email FROM user WHERE email = ? AND password = ?");
  const row = stmt_select.get(req.body.email, md5(req.body.password));
  if (row) {
    res.status(200).json({
      "message":"success",
      "data":token.create_token(row)
    })
  } else {
    res.status(500).json({
      "message":"failure"
    })
  }
});

app.post('/signup', function(req, res){
  console.log("email : " + req.body.email + " passwd : " + req.body.password);
  var stmt_insert = db.prepare("INSERT INTO user (email, password) VALUES (?, ?)");
  stmt_insert.run(req.body.email, md5(req.body.password));
  res.status(200).json({"message": "success"});
});

//get all documents from a user
app.get('/documents', function(req, res){

  if (token.check_token(req.headers.authorization.split(' ')[1])) {
    var email = token.get_email_from_token(req.headers.authorization.split(' ')[1]);
    var response_json = {};

    var stmt_artworks = db.prepare("SELECT id, name, desc, author FROM artwork WHERE user_id = ( \
       SELECT id FROM user WHERE email = ? \
       )");
    const rows = stmt_artworks.all(email)
    for (row of rows) {
      response_json[row.id] = row;

      var stmt_pictures = db.prepare("SELECT path FROM picture WHERE artwork_id = ?");
      var row_ = stmt_pictures.get(row.id);
      if (row_) {
        response_json[row.id]['path'] = row_['path'];
      }
    }
    res.status(200).send(JSON.stringify(response_json));
  }
  res.status(500).json({"message": "failure"});
});

app.get('/documents/:id', function(req, res){
  /*
  console.log("token : " + req.headers.authorization.split(' ')[1] + " id_document : " + req.body.id_document);
  var sql_select = db.prepare("SELECT document FROM document JOIN user WHERE user.email = ? AND id_document = ?");
  var row = sql_select.get(token.get_email_from_token(req.headers.authorization.split(' ')[1]), req.params.id);
  */
  if (token.check_token(req.headers.authorization.split(' ')[1])) {
    var ownership = interface.getDocument(req.params.id);
    res.status(200).json({
      "message": "success",
      "data": ownership
    });
  }
  res.status(500).json({"message": "failure"});
});

app.post('/documents', function(req, res){
  console.log("token : " + req.headers.authorization.split(' ')[1] + " images : " + req.body.images + " pdf : " + req.body.pdf);
  if (token.check_token(req.headers.authorization.split(' ')[1])) {
    var email = token.get_email_from_token(req.headers.authorization.split(' ')[1]);
    var stmt_select = db.prepare("SELECT id FROM user WHERE email = ?");
    var row = stmt_select.get(email);

    var stmt_insert = db.prepare("INSERT INTO artwork (user_id, name) VALUES (?, ?)");
    var info = stmt_insert.run(row.id, req.body.name);
    var art_id = info.lastInsertRowid;

       //loop all files
    if (req.files.images.length) {
      _.forEach(_.keysIn(req.files.images), (key) => {
        let photo = req.files.images[key];
        photo.mv('./pictures/' + photo.name);

        var stmt_insert = db.prepare("INSERT INTO picture (artwork_id, path) VALUES (?, ?)");
        var info = stmt_insert.run(art_id, photo.name);
     });
    } else {
      let photo = req.files.images;
      photo.mv('./pictures/' + photo.name);
      var stmt_insert = db.prepare("INSERT INTO picture (artwork_id, path, is_main) VALUES (?, ?, 1)");
      var info = stmt_insert.run(art_id, photo.name);
    }

    req.files.pdf.mv('./tmp/' + req.files.pdf.name);
    var stmt_insert = db.prepare("INSERT INTO picture (artwork_id, path, is_main) VALUES (?, ?, 1)");
    interface.addDocumentToBlockchain(art_id, row.id, "./tmp/" + req.files.pdf.name, req.body.name);

    res.status(200).json({"message": "success"});
  } else {
    res.status(500).json({"message": "failure"});
  }
});

app.delete('/documents/:id', function(req, res){
  var stmt_delete = db.prepare("DELETE FROM artwork WHERE id = ?");
  stmt_delete.run(req.params.id);
  res.status(200).json({"message": "success"});
});

/*
app.patch('/documents/:id', function(req, res) {
  res.end('your id is ' + req.params.id);
});

app.patch('/documents', function(req, res){

});

app.post('/documents/:id/share', function(req, res){

});
*/
app.listen(8080, 'localhost');
