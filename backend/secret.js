const fs = require('fs')

var secret = fs.readFileSync('secret', 'utf8', (err, data) => {
  if (err) {
    console.err(err);
    return;
  }
  return data;
});

module.exports = { secret };
