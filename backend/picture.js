const fs = require('fs')

function load_picture(name) {
  fileToLoad = fs.readFileSync("pictures/" + name);
  return fileToLoad;
}

module.exports = { load_picture };
