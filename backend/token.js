var jws = require("jws");

var secret = require('./secret.js');

/*
function base64url(source) {
  // Encode in classical base64
  encodedSource = CryptoJS.enc.Base64.stringify(source);

  // Remove padding equal characters
  encodedSource = encodedSource.replace(/=+$/, '');

  // Replace characters according to base64url specifications
  encodedSource = encodedSource.replace(/\+/g, '-');
  encodedSource = encodedSource.replace(/\//g, '_');

  return encodedSource;
}
*/

var header = {
  "alg": "HS256",
  "typ": "JWT"
};
/*
//data is a "struct" with id and name
function create_token(data) {
  var stringifiedHeader = CryptoJS.enc.Utf8.parse(JSON.stringify(header));
  var encodedHeader = base64url(stringifiedHeader);

  var stringifiedData = CryptoJS.enc.Utf8.parse(JSON.stringify(data));
  var encodedData = base64url(stringifiedData);

  var token = encodedHeader + "." + encodedData;

  var signature = CryptoJS.HmacSHA256(token, secret.secret);
  signature = base64url(signature);

  var signedToken = token + "." + signature;
  return signedToken;
}
*/

function create_token(data) {
  var token = jws.sign({
    header: header,
    payload: data,
    secret: secret.secret,
  });
  return token;
  }

function check_token(token) {
  return jws.verify(token, 'HS256', secret.secret);
}

function decode_token(token) {
  return jws.decode(token);
}

function get_email_from_token(token) {
  const obj = jws.decode(token);
  return obj.payload.email;
}

module.exports = { create_token , check_token, get_email_from_token};
