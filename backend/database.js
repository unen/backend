var sqlite3 = require('sqlite3').verbose()
var md5 = require('md5')

const DBSOURCE = "db.sqlite"

let db = new sqlite3.Database(DBSOURCE, (err) => {
  if (err) {
    // Cannot open database
    console.error(err.message)
    throw err
  } else {
    console.log('Connected to the SQLite database.')
    db.run(`CREATE TABLE user (
           id INTEGER PRIMARY KEY AUTOINCREMENT,
           name text,
           email text UNIQUE,
           password text,
           CONSTRAINT email_unique UNIQUE (email)
           )`,
           (err) => {
             if (err) {
               // Table already created
             } else {
               // Table just created, creating some rows
               var insert = 'INSERT INTO user (name, email, password) VALUES (?,?,?)'
               db.run(insert, ["admin","admin@example.com",md5("admin123456")])
               db.run(insert, ["user","user@example.com",md5("user123456")])
             }
    });
    db.run(`CREATE TABLE artwork (
           id INTEGER PRIMARY KEY AUTOINCREMENT,
           name TEXT,
           desc TEXT,
           author TEXT,
           date TIME,
           user_id INTEGER,
           FOREIGN KEY(user_id) REFERENCES user(id)
           )`,
           (err) => {
              if (err) {
                //already exists
              } else {
                var insert = 'INSERT INTO artwork (desc, name, user_id) VALUES (?,?,?)'
                db.run(insert, ["desc_placeholder","name_placeholder", 1])
                db.run(insert, ["ad","ad", 1]);
              }
    });
    db.run(`CREATE TABLE picture (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          path TEXT,
          artwork_id INTEGER,
          is_main INTEGER,
          FOREIGN KEY(artwork_id) REFERENCES artwork(id)
          )`,
          (err) => {
            if (err) {
              //already exists
            } else {
              var insert = 'INSERT INTO picture (artwork_id, path, is_main) VALUES (?,?,?)'
              db.run(insert, [1,"example.jpg", 1]);
              db.run(insert, [1,"example2.jpg", 0]);
              db.run(insert, [2,"ex.jpg", 1])
            }
    });
  }
});


module.exports = db
