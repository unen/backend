const Web3 = require("web3");
const configuration = require('../build/contracts/Unen.json');

const CONTRACT_ADDRESS = configuration.networks["1648755218222"].address;
const CONTRACT_ABI = configuration.abi;

const web3 = new Web3(Web3.givenProvider || 'http://127.0.0.1:8545');
const contract = new web3.eth.Contract(CONTRACT_ABI, CONTRACT_ADDRESS);

/**
 * Create ownership transaction on the blockchain
 * @param {string} ref ownership reference, needs to be unique
 * @param {string} ownerId owner of the document
 * @param {string} cid cid of the document
 */
module.exports.addOwnerShip = async function addOwnerShip(ref, ownerId, cid) {
    const accounts = await web3.eth.getAccounts();
    return contract.methods.addOwnerShip(ref, ownerId, cid).send({
        from: accounts[0],
        gas: 3000000
    });
}

/**
 * Transfer ownership from client to an other.
 * @param {string} ref ownership reference
 * @param {string} newOwnerId new owner of the document
 */
module.exports.transferOwnerShip = async function transferOwnerShip(ref, newOwnerId) {
    const accounts = await web3.eth.getAccounts();
    return contract.methods.transferOwnerShip(ref, newOwnerId).send({
        from: accounts[0],
        gas: 3000000
    });
}

/**
 * Delete ownership from client to an other.
 * @param {string} ref ownership reference
 */
 module.exports.deleteOwnerShip = async function deleteOwnerShip(ref) {
    const accounts = await web3.eth.getAccounts();
    return contract.methods.deleteOwnerShip(ref).send({
        from: accounts[0],
        gas: 3000000
    });
}

/**
 * Allows to get ownership
 * @param {string} ref ref ownership reference
 * @returns the ownership
 */
module.exports.getOwnerShip = async function getOwnerShip(ref) {
    const accounts = await web3.eth.getAccounts();
    return contract.methods.getOwnerShip(ref).call({
        from: accounts[0],
        gas: 3000000
    });
}