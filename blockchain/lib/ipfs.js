const axios = require('axios');
const fs = require('fs');
const FormData = require('form-data');
const crypto = require('crypto');

const algorithm = 'aes-256-ctr';
const secretKey = 'vOVH6sdmpNWjRRIqCc7rdxs01lwHzfr3';
const iv = crypto.randomBytes(16);

/**
 * Upload a file on ipfs by pining it to pinata
 * @param {string} path 
 * @param {string} name 
 * @param {map<string, dynamic>} keyvalues 
 * @returns The CID
 */
module.exports.addFileToIPFS = async function addFileToIPFS(path, name, keyvalues) {
    const url = `https://api.pinata.cloud/pinning/pinFileToIPFS`;

    /*
    const file = await fs.readFileSync(path);
    const encryptedData = encrypt(file);
    const encryptedFile = fs.writeFileSync("./encrypted.txt", encryptedData);
    */

    //we gather a local file for this example, but any valid readStream source will work here.
    let data = new FormData();
    data.append('file', fs.createReadStream(path));

    //You'll need to make sure that the metadata is in the form of a JSON object that's been convered to a string
    //metadata is optional
    const metadata = JSON.stringify({
        name: name,
        keyvalues: keyvalues
    });
    data.append('pinataMetadata', metadata);

    //pinataOptions are optional
    const pinataOptions = JSON.stringify({
        cidVersion: 0,
        customPinPolicy: {
            regions: [{
                id: 'FRA1',
                desiredReplicationCount: 1
            }]
        }
    });
    data.append('pinataOptions', pinataOptions);

    const response = await axios
        .post(url, data, {
            maxBodyLength: 'Infinity', //this is needed to prevent axios from erroring out with large files
            headers: {
                'Content-Type': `multipart/form-data; boundary=${data._boundary}`,
                pinata_api_key: "631bf657eae6c47d00e6",
                pinata_secret_api_key: "16a12a311706abf732f0fbaf995a001b14e9bf0f5ec13282f0d0f2600cd3771e"
            }
        })
    return response.data.IpfsHash;
}

const encrypt = (buffer) => {

    const cipher = crypto.createCipheriv(algorithm, secretKey, iv);

    const encrypted = Buffer.concat([cipher.update(buffer), cipher.final()]);

    return encrypted;
};

const decrypt = (hash) => {

    const decipher = crypto.createDecipheriv(algorithm, secretKey, Buffer.from(hash.iv, 'hex'));

    const decrypted = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()]);

    return decrypted.toString();
};