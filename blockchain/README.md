# BLOCKCHAIN
## Setup
Type `npm install ganache --global` to install the ganache cli.
Start the blockchain by typing `ganache-cli`.

Open a new terminal.
Install the truffle tools with `npm install -g truffle`.

Now go to *blockchain/exemple* and run `python3 launch_truffle.py`

## Try 
To try it, run `node complete.js`

You should see lines like these :
>CID ->  QmXgm7EnB8RSLiMVdh5z52iAtJf5zLUuPbXKqDQJtseck8
>[ '1', 'dwdwd', clientId: '1', cid: 'QmXgm7EnB8RSLiMVdh5z52iAtJf5zLUuPbXKqDQJtseck8' ]
>[ '2', 'dwdwd', clientId: '2', cid: 'QmXgm7EnB8RSLiMVdh5z52iAtJf5zLUuPbXKqDQJtseck8' ]
>[ '', '', clientId: '', cid: '' ]




