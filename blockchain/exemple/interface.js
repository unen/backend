const ipfs = require("../lib/ipfs");
const blockchain = require("../lib/blockchain");

const addDocumentToBlockchain = async (artwork_id, user_id, document, name) => {

    // Upload a file on IPFS
    const CID = await ipfs.addFileToIPFS(document, name, {
        artworkdID: artwork_id
    });
    console.log("CID -> ", CID);

    // Register the ownership on the blockchain
    //await blockchain.addOwnerShip(artwork_id, user_id.toString(), artwork_id.toString());
    await blockchain.addOwnerShip(artwork_id, user_id.toString(), CID);
    let ownership = await blockchain.getOwnerShip(artwork_id);
    console.log(ownership);
/*
    // Transfer the ownership to an other user
    await blockchain.transferOwnerShip("aaaa", "2");
    ownership = await blockchain.getOwnerShip("aaaa");
    console.log(ownership);
*/
}

const getDocument = async (artwork_id) => {
  let ownership = await blockchain.getOwnerShip(parseInt(artwork_id));
  console.log(ownership);
  return ownership;
}

module.exports = { addDocumentToBlockchain, getDocument };
