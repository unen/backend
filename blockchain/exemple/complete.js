const ipfs = require("../lib/ipfs");
const blockchain = require("../lib/blockchain");

const main = async () => {

    // Upload a file on IPFS
    const CID = await ipfs.addFileToIPFS("./nft.jpeg", "nft", {
        exampleKey: 'exampleValue'
    });
    console.log("CID -> ", CID);

    // Register the ownership on the blockchain
    await blockchain.addOwnerShip("aaaa", "1", CID);
    let ownership = await blockchain.getOwnerShip("aaaa");
    console.log(ownership);

    // Transfer the ownership to an other user
    await blockchain.transferOwnerShip("aaaa", "2");
    ownership = await blockchain.getOwnerShip("aaaa");
    console.log(ownership);

    // Delete ownership
    let result = await blockchain.deleteOwnerShip("aaaa");
    ownership = await blockchain.getOwnerShip("aaaa");
    console.log(ownership);
}

main();