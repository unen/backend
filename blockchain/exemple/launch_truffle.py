import json
import os
import shutil

shutil.rmtree("../build", ignore_errors = True)
os.system("truffle migrate")

with open('../build/contracts/Unen.json', 'r') as f:
  data = json.load(f)

for key, _ in data["networks"].items():
    network_id = key
    break

with open('../lib/blockchain.js', 'r') as file:
    data = file.readlines()

data[3] = "const CONTRACT_ADDRESS = configuration.networks[\"" + network_id + "\"].address;\n"

with open('../lib/blockchain.js', 'w') as file:
    file.writelines( data )
