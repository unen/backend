// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "@openzeppelin/contracts/access/Ownable.sol";

contract Unen is Ownable {
    struct OwnerShip {
        string clientId;
        string cid;
    }

    mapping(string => OwnerShip) private ownerShips;

    function addOwnerShip(
        string memory ref,
        string memory clientId,
        string memory cid
    ) external onlyOwner {
        ownerShips[ref] = OwnerShip(clientId, cid);
    }

    function transferOwnerShip(string memory ref, string memory newClientId)
        external
        onlyOwner
    {
        ownerShips[ref].clientId = newClientId;
    }

     function deleteOwnerShip(string memory ref)
        external
        onlyOwner
        returns (bool)
    {
        delete ownerShips[ref];
        return true;
    }

    function getOwnerShip(string memory ref)
        external
        view
        returns (OwnerShip memory)
    {
        return ownerShips[ref];
    }
}
